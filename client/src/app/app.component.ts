import { Component } from '@angular/core';
import { User } from './models/user';
import { AccountserviceService } from './services/accountservice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';

  constructor(private accountService:AccountserviceService){}
  ngOnInit(){
this.setCurrentUser();
  }

  setCurrentUser(){
    const user: User = JSON.parse(localStorage.getItem('user') ?? '');
    this.accountService.setCurrentUser(user);
  }

}
