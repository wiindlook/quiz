import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  baseUrl = 'https://localhost:44393/';
  constructor(private http:HttpClient) { }

  getQuizes() {
    return this.http.get(this.baseUrl + 'quiz/getquizes');
  }

  createQuiz(quiz:any)
  {
   return this.http.post(this.baseUrl + 'quiz/createquizes', quiz);
  }

  updateQuiz(quiz:any)
  {
   return  this.http.put(this.baseUrl + 'quiz/updatequiz', quiz);
  }
}
