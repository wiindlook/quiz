import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatequizComponent } from './pages/createquiz/createquiz.component';
import { MainpageComponent } from './pages/mainpage/mainpage.component';

const routes: Routes = [
  {path:'',component:MainpageComponent},
  {path:'createquiz',component:CreatequizComponent},
  {path: '**', component: MainpageComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
