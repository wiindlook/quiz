import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-editquiz',
  templateUrl: './editquiz.component.html',
  styleUrls: ['./editquiz.component.scss']
})
export class EditquizComponent implements OnInit {
inputQuiz:any;
editForm:FormGroup;
  constructor(public modal: NgbActiveModal,private formBuilder:FormBuilder,private quizService:QuizService) { }

  ngOnInit(): void {
    this.setForm();
  }

  onSubmit(){
    this.quizService.updateQuiz(this.editForm.value).subscribe(x=>{
      this.modal.close('Yes');
    },
    error=>{
      console.log(error);
    })
  }
  private setForm() {
    console.log(this.inputQuiz);
     
    this.editForm = this.formBuilder.group({
      id:[this.inputQuiz.id],
      name: [this.inputQuiz.name],
      topic: [this.inputQuiz.topic],
      description: [this.inputQuiz.description],
    });
  }
}
