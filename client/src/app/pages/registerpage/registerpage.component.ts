import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AccountserviceService } from 'src/app/services/accountservice';

@Component({
  selector: 'app-registerpage',
  templateUrl: './registerpage.component.html',
  styleUrls: ['./registerpage.component.scss']
})
export class RegisterpageComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  model: any = {};
  currentUser$:any;

  constructor(private accountService: AccountserviceService) { }

  ngOnInit(): void {

  }


  register() {
    this.accountService.register(this.model).subscribe(response => {
      console.log(response);
      this.cancel();
    }, error => {
      console.log(error);
    })
  }

  cancel() {
    this.cancelRegister.emit(false);
  }


}
