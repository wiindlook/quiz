import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QuizService } from 'src/app/services/quiz.service';
import { EditquizComponent } from '../editquiz/editquiz.component';

@Component({
  selector: 'app-quizes',
  templateUrl: './quizes.component.html',
  styleUrls: ['./quizes.component.scss']
})
export class QuizesComponent implements OnInit {
quizes:any;
  constructor(private quizService:QuizService,private modalService:NgbModal) { }

  ngOnInit(): void {
    this.getQuiz();
  }

  getQuiz()
  {
    this.quizService.getQuizes().subscribe(quiz=>{
      this.quizes=quiz;
    })
  }

  editQuiz(quiz:any) {
    const ref = this.modalService.open(EditquizComponent, { centered: true });
    ref.componentInstance.inputQuiz = quiz;
  
    ref.result.then((yes) => {
     this.getQuiz();
    },
      (cancel) => {
        console.log("Cancel Click");
      })
  }
}

 class QuizData
{
  Id:string;
  Name: string;
  Topic:string;
  Description:string;
  Questions:Array<QuestionData>;
  constructor(id:string,name:string,description:string,topic:string,questions:Array<QuestionData>)
  {
    this.Id=id;
    this.Name=name;
    this.Topic=topic;
    this.Description=description;
    this.Questions=questions
  }
}
class QuestionData{
Id:string;
QuizId:string;
Text:string;
constructor(id:string,quizId:string,text:string)
{
this.Id=id;
this.QuizId=quizId;
this.Text=text;
}
}
