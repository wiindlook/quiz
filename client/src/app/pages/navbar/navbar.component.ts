import { Component, OnInit } from '@angular/core';
import { AccountserviceService } from 'src/app/services/accountservice';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  model:any={}
  toggleFlag:boolean=false;
  constructor(public accountService:AccountserviceService) { }

  ngOnInit(): void {
  }

login(){
  this.accountService.login(this.model).subscribe(response=>{
    console.log(response);
  },error=>{
    console.log(error);
  })
}
logout()
{
  this.accountService.logout();
}
clicked()
{
this.toggleFlag=!this.toggleFlag;
}

}
