import { Component, OnInit } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-createquiz',
  templateUrl: './createquiz.component.html',
  styleUrls: ['./createquiz.component.scss']
})
export class CreatequizComponent implements OnInit {
  quiz:QuizData={
    Name: '',
    Topic: '',
    Description: '',
    Questions: []
  };
  questionsTemplate:Array<any>=[];

  constructor(private quizService:QuizService) { }

  ngOnInit(): void {
  }

  createQuizes()
  {
    this.addQuestionsToquiz();
    this.quizService.createQuiz(this.quiz).subscribe(result=>{
      console.log(result);
    });
  }
  addQuestionsToquiz()
  {
    this.questionsTemplate.forEach(questionTemplate=>{
      this.quiz.Questions.push(questionTemplate);
    })
  }
  removeQuestion(i:number){
    this.questionsTemplate.splice(i,1);
  }
  addQuestions() {
    this.questionsTemplate.push(
      {
      Text:''
    });
  }
}
class QuizData
{
  Name: string;
  Topic:string;
  Description:string;
  Questions:Array<QuestionData>;
  constructor(id:string,name:string,description:string,topic:string,questions:Array<QuestionData>)
  {
    this.Name=name;
    this.Topic=topic;
    this.Description=description;
    this.Questions=questions
  }
}
class QuestionData{
Text:string;
constructor(text:string)
{
this.Text=text;
}
}

