import { Component, Input, OnInit } from '@angular/core';
import { AccountserviceService } from 'src/app/services/accountservice';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss']
})
export class MainpageComponent implements OnInit {

registerMode = false;
isClicked:boolean=true;

  constructor(public accountService :AccountserviceService) { }

  ngOnInit(): void {
  }


  clicked()
  {
    this.isClicked=!this.isClicked;
  }


  registerToggle() {
    this.registerMode = !this.registerMode;
  }

  cancelRegisterMode(event:any) {
    this.registerMode = event;
  }
}
