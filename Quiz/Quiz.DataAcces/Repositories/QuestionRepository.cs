﻿using Common.Models;
using DataAcces.Interfaces;
using Quiz.DataAcces.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly DataContext _context;
        public QuestionRepository(DataContext context)
        {
            this._context = context;
        }
        public List<Question> GetQuestions() => _context.Question.ToList();
        public Question GetQuestionById(Guid quiestionId) => _context.Question.FirstOrDefault(x => x.Id == quiestionId);

        public IQueryable<Question> GetQuestionsByQuizId(Guid quizId) => _context.Question.Where(x => x.QuizId == quizId);

        public void Create(Question question)
        {
            _context.Question.Add(question);
            _context.SaveChanges();
        }

        public void Update(Question question)
        {
            _context.Question.Update(question);
            _context.SaveChanges();
        }
        public void Delete(Question question)
        {
            _context.Question.Add(question);
            _context.SaveChanges();
        }

        public void DeleteById(Guid id)
        {
            var question = GetQuestionById(id);
            _context.Question.Remove(question);
            _context.SaveChanges();
        }


    }
}
