﻿using DataAcces.Interfaces;
using Quiz.Common.Models;
using Quiz.DataAcces.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context)
        {
            this._context = context;
        }

        public void CreateUser(User user)
        {
            _context.User.Add(user);
            _context.SaveChanges();
        }


        public void DeleteUserById(Guid id)
        {
            var user = GetUserById(id);
            _context.User.Remove(user);
            _context.SaveChanges();
        }


        public User GetUserById(Guid id) =>_context.User.FirstOrDefault(x => x.Id == id);

        public User GetUserByUsername(string username)=> _context.User.SingleOrDefault(x => x.Username == username);
        

        public List<User> GetUsers() => _context.User.ToList();


        public void UpdateUser(User user)
        {
            _context.User.Update(user);
            _context.SaveChanges();
        }
    }
}
