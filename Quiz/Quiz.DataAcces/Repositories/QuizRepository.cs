﻿using Common.Models;
using DataAcces.Interfaces;
using Quiz.DataAcces.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
    public class QuizRepository : IQuizRepository
    {
        private readonly DataContext _context;
        public QuizRepository(DataContext context)
        {
            this._context = context;        
        }

        public List<Quizul> GetQuizes() => _context.Quiz.ToList();

        public Quizul GetQuizById(Guid quizId) => _context.Quiz.FirstOrDefault(x => x.Id == quizId);


        public void Create(Quizul quiz)
        {
            _context.Quiz.Add(quiz);
            _context.SaveChanges();
        }

        public void Update(Quizul quiz)
        {
            _context.Update(quiz);
            _context.SaveChanges();
        }

        public void Delete(Quizul quiz)
        {
            _context.Quiz.Remove(quiz);
            _context.SaveChanges();
        }

        public void DeleteById(Guid id)
        {
            var quiz = GetQuizById(id);
            _context.Quiz.Remove(quiz);
            _context.SaveChanges();
        }

    }

}
