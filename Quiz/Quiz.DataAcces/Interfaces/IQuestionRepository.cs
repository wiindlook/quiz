﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IQuestionRepository
    {
        List<Question> GetQuestions();
        Question GetQuestionById(Guid quiestionId);

        IQueryable<Question> GetQuestionsByQuizId(Guid quizId);

        void Create(Question question);
        void Update(Question question);

        void DeleteById(Guid id);

        void Delete(Question question);
    }
}
