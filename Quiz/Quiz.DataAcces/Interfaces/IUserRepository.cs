﻿using Quiz.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
   public interface IUserRepository
    {
        List<User> GetUsers();
        User GetUserById(Guid id);

        User GetUserByUsername(string username);
        void CreateUser(User user);
        void UpdateUser(User user);

        void DeleteUserById(Guid id);

    }
}
