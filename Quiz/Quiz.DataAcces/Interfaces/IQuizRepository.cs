﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IQuizRepository
    {
        List<Quizul> GetQuizes();
        Quizul GetQuizById(Guid quizId);

        void Create(Quizul quiz);
        void Update(Quizul quiz);

        void DeleteById(Guid id);
        void Delete(Quizul quiz);
        

    }
}
