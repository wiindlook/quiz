﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
  public interface IQuizService
    {
        List<Quizul> GetQuizes();
        Quizul GetQuizById(Guid quizId);
        public Quizul GetQuizWithQuestions(Guid quizID);

        void Create(Quizul quiz);
        void Update(Quizul quiz);

        void DeleteById(Guid id);
        void Delete(Quizul quiz);
    }
}
