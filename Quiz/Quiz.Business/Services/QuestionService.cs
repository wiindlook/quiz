﻿using Business.Interfaces;
using Common.Models;
using DataAcces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;
        public QuestionService(IQuestionRepository questionRepository)
        {
            this._questionRepository = questionRepository;
        }
        public void Create(Question question)
        {
            _questionRepository.Create(question);
        }

        public void Delete(Question question)
        {
            _questionRepository.Delete(question);
        }

        public void DeleteById(Guid id)
        {
            _questionRepository.DeleteById(id);
        }

        public Question GetQuestionById(Guid quiestionId)
        {
           return _questionRepository.GetQuestionById(quiestionId);
        }

        public IQueryable<Question> GetQuestionsByQuizId(Guid quizId)
        {
            return _questionRepository.GetQuestionsByQuizId(quizId);
        }

        public List<Question> GetQuestions()
        {
            return _questionRepository.GetQuestions();
        }

        public void Update(Question question)
        {
            _questionRepository.Update(question);
        }
    }
}
