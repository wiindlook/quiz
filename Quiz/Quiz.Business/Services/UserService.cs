﻿using Business.Interfaces;
using DataAcces.Interfaces;
using DataAcces.Repositories;
using Quiz.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
  
    public class UserService :IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public void CreateUser(User user)
        {
            _userRepository.CreateUser(user);
        }

        public void DeleteUserById(Guid id)
        {
            _userRepository.DeleteUserById(id);
        }

        public User GetUserById(Guid id)
        {
           return _userRepository.GetUserById(id);
        }

        public List<User> GetAllUsers()
        {
           return _userRepository.GetUsers();
        }

        public void UpdateUser(User user)
        {
            _userRepository.UpdateUser(user);
        }

        public User GetUserByUsername(string username)
        {
           return _userRepository.GetUserByUsername(username);
        }
    }
}
