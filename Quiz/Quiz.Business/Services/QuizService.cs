﻿using Business.Interfaces;
using Common.Models;
using DataAcces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class QuizService : IQuizService
    {
        private readonly IQuizRepository _quizRepository;
        private readonly IQuestionRepository _questionRepository;

        public QuizService(IQuizRepository quizRepository, IQuestionRepository questionRepository)
        {
            this._quizRepository = quizRepository;
            this._questionRepository = questionRepository;
        }
        public void Create(Quizul quiz)
        {
            _quizRepository.Create(quiz);
        }

        public void Delete(Quizul quiz)
        {
            _quizRepository.Delete(quiz);
        }

        public void DeleteById(Guid id)
        {
            _quizRepository.DeleteById(id);
        }

        public Quizul GetQuizById(Guid quizId)
        {
            return _quizRepository.GetQuizById(quizId);
        }

        public List<Quizul> GetQuizes()
        {
            var quizes= _quizRepository.GetQuizes();
            foreach(var quiz in quizes)
            {
                AddQuestionsToQuiz(quiz);
            }
            return quizes;
        }

        public void Update(Quizul quiz)
        {
            _quizRepository.Update(quiz);
        }
        public Quizul GetQuizWithQuestions(Guid quizId)
        {
            var quiz = GetQuizById(quizId);
            AddQuestionsToQuiz(quiz);
            return quiz;
        }
        private void AddQuestionsToQuiz(Quizul quiz)
        {
            var questions = _questionRepository.GetQuestionsByQuizId(quiz.Id).ToList();
            foreach (var question in questions)
            {
                quiz.Questions.Add(question);
            }
        }
    }
}
