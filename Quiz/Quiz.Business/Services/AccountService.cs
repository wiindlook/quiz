﻿using Business.Interfaces;
using DataAcces.Interfaces;
using Quiz.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }
        public void Register(User user)
        {
            _accountRepository.CreateUser(user);
        }
    }
}
