﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class Quizul
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(30,MinimumLength =5)]
        public string Name { get; set; }
        public string Topic { get; set; }

        public string Description { get; set; }
        public virtual List<Question> Questions { get; set; } = new List<Question>();
    }
}
