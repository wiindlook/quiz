﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class Question
    {
        public Guid Id { get; set; }

        public Guid QuizId { get; set; }
        [Required]
        [StringLength(200,MinimumLength =10)]
        public string Text { get; set; }

        public virtual Quizul Quiz { get; set; }

    }
}
