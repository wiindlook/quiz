﻿using AutoMapper;
using Business.Interfaces;
using Business.TokenService;
using Microsoft.AspNetCore.Mvc;
using Quiz.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTOS;

namespace Web.Controllers
{
    public class AccountController : ControllerOptions
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        public AccountController(IAccountService accountService, IUserService userService, IMapper mapper, ITokenService tokenService)
        {
            _accountService = accountService;
            _userService = userService;
            _mapper = mapper;
            _tokenService = tokenService;
        }

        [HttpPost("Register")]
        public ActionResult<UserProfileDto> RegisterUser(UserRegisterDto userRegisterDto)
        {
            var user = new User
            {
                Age = userRegisterDto.Age,
                Email = userRegisterDto.Email,
                Username = userRegisterDto.Username,
                Password = userRegisterDto.Password
            };
            _accountService.Register(user);

            return new UserProfileDto
            {
                Username = user.Username,
                Age = user.Age,
                Email = user.Email,
                Token = _tokenService.CreateToken(user)
            };
        }

        [HttpPost("Login")]
        public ActionResult<UserProfileDto> LoginUser(LoginDto loginDto)
        {
           if(loginDto is null)
            {
                return NotFound();
            }
            var user = _userService.GetUserByUsername(loginDto.Username);
            if (loginDto.Password != user.Password)
            {
                return Unauthorized("ce faci boss? nu mai tii minte?");
            }

            return new UserProfileDto
            {
                Username = user.Username,
                Age = user.Age,
                Email = user.Email,
                Token = _tokenService.CreateToken(user)
        };
    
        }




    }
}
