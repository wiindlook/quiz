﻿using Business.Interfaces;
using Business.Services;
using Microsoft.AspNetCore.Mvc;
using Quiz.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTOS;
using Web.ExtensionMethods;

namespace Web.Controllers
{
    public class UserController : ControllerOptions
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            this._userService = userService;
        }

        [HttpGet]
        public IActionResult GetUsers()
        {
            var result = _userService.GetAllUsers();

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetUserWithId(Guid id)
        {
            var user = _userService.GetUserById(id);
            return Ok(user);
        }

        [HttpPost]
        public IActionResult RegisterUser(UserRegisterDto userRegisterDto)
        {
            var user = new User
            {
                Age = userRegisterDto.Age,
                Email = userRegisterDto.Email,
                Username = userRegisterDto.Username,
               Password = userRegisterDto.Password
            };
            _userService.CreateUser(user);
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateUser([FromBody]UserProfileDto userProfileDto)
        {
            _userService.UpdateUser(userProfileDto.ToDomain());
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteUserWithId(Guid id)
        {
            _userService.DeleteUserById(id);
            return Ok();
        }
        

       
    }
}
