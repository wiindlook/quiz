﻿using AutoMapper;
using Business.Interfaces;
using Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTOS;

namespace Web.Controllers
{
    public class QuestionController : ControllerOptions
    {
        private readonly IQuestionService _questionService;
        private readonly IMapper _mapper;
        public QuestionController(IQuestionService questionService, IMapper mapper)
        {
            this._questionService = questionService;
            this._mapper = mapper;
        }
        
        [HttpGet]
        public IActionResult GetQuestions()
        {
            var questions = _questionService.GetQuestions();
            return Ok(questions);
        }

        [HttpGet("question/{id}")]
        public IActionResult GetQuestionById(Guid id)
        {
            var question = _questionService.GetQuestionById(id);
            return Ok(question);
        }

        [HttpGet("{quizId}")]
        public IActionResult GetQuizByQuizId(Guid id)
        {
            var question = _questionService.GetQuestionsByQuizId(id);
            return Ok(question);
        }

        [HttpPost]
        public IActionResult AddQuestionToQuiz([FromBody]QuestionDto questionDto)
        {
            var question = new Question
            {
                QuizId=questionDto.QuizId,
                Text=questionDto.Text
            };
            _questionService.Create(question);
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateQuestion([FromBody]QuestionDto questionDto)
        {

            var question = MapQuestionToUpdate(questionDto);
            _questionService.Update(question);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteQuestion(QuestionDto questionDto)
        {
            var questionToBeDeleted = _questionService.GetQuestionById(questionDto.Id);
            _questionService.Delete(questionToBeDeleted);

            return Ok();
        }

        [HttpDelete("{id}")]
        public  IActionResult DeleteById(Guid id)
        {
            _questionService.DeleteById(id);
            return Ok();

        }
        
        private Question MapQuestionToUpdate(QuestionDto questionDto)
        {
            var question = _questionService.GetQuestionById(questionDto.Id);
            question.QuizId = questionDto.QuizId;
            question.Text = questionDto.Text;

            return question;
        }
    }
}
