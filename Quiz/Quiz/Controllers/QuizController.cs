﻿using AutoMapper;
using Business.Interfaces;
using Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTOS;

namespace Web.Controllers
{
    public class QuizController : ControllerOptions
    {
        private readonly IQuizService _quizService;
        private readonly IMapper _mapper;

        public QuizController(IQuizService quizService, IMapper mapper)
        {
            this._quizService = quizService;
            this._mapper = mapper;
        }
        
        /// <summary>
        /// Gets All Quizes
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetQuizes")]
        public IActionResult GetQuizes()
        {
            var quizes = _quizService.GetQuizes();
            var quizesDto = _mapper.Map < List<QuizDto>>(quizes);
            return Ok(quizesDto);
        }

        /// <summary>
        /// Gets quiz by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]     
        public IActionResult GetQuizById(Guid id)
        {
            var quiz = _quizService.GetQuizWithQuestions(id);

            var quizDto=_mapper.Map<QuizDto>(quiz);
            return Ok(quizDto);
        }

        [HttpPost("CreateQuizes")]
        public IActionResult CreateQuiz([FromBody]QuizDtoCreation quizDto)
        {
            var quiz = _mapper.Map<Quizul>(quizDto);
            _quizService.Create(quiz);
            return Ok();
        }
        [HttpPut("updatequiz")]
        public IActionResult UpdateQuiz(QuizDto quizDto)
        {

         var quiz= UpdatedQuiz(quizDto);
            _quizService.Update(quiz);
            return Ok();
        }
        [HttpDelete]
        public IActionResult DeleteQuiz(Guid id)
        {
            _quizService.DeleteById(id);
            return Ok();
        }
        private Quizul UpdatedQuiz(QuizDto quizDto)
        {
            var quiz = _quizService.GetQuizById(quizDto.Id);
            quiz.Name = quizDto.Name;
            quiz.Description = quizDto.Description;
            quiz.Topic = quizDto.Topic;
            return quiz;
        }
    }
}
