﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTOS
{
    public class QuizDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Topic { get; set; }

        public string Description { get; set; }

        public virtual List<QuestionDto> Questions { get; set; }
    }
}
