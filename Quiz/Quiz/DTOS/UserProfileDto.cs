﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTOS
{
    public class UserProfileDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }

        public int Age { get; set; }
    }
}
