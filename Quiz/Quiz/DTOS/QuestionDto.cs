﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTOS
{
    public class QuestionDto
    {
        public Guid Id { get; set; }

        public Guid QuizId { get; set; }
        public string Text { get; set; }

    }
}
